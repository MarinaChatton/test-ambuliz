versionsOrder
=====

Le but est d'écrire la fonction **versionsOrder** qui reçoit un tableau de chaîne de caractères de numéro de version de logiciel 
et doit retourner le même tableau trié de la plus ancienne version à la plus récente.

Par exemple: 
```js
import { versionsOrder } from 'versions-order';

const versions = [
  '1.0.1',
  '0.2.1',
  '5.0',
  '3',
  '1.0.0',
];

console.log(versionsOrder(versions))

// Affichera
/*
[
  '0.2.1',
  '1.0.0',
  '1.0.1',
  '3',
  '5.0',
]
*/
```

Vous ne devez pas utiliser de dépendences (pas de lodash, ...).
Le code n'a pas besoin d'être optimisé en complexité (de temps ou d'espace). Il vaut mieux privilégié un code lisible et pourquoi pas bien testé.

Libre à vous de nous joindre votre résultat soit :
- En faisant directement un fork de ce gist et en ajoutant le code au fichier versions-order.js
- En créant un repo git
- En créant un .zip avec votre source code
- En créant un projet sur https://codesandbox.io ou autres plateformes du même type
- Ou tout autre moyen...

Merci d'envoyer votre solution à thomas@ambuliz.com

