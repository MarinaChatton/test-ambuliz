versionsOrder = versions => versions.sort(compareVersions);

// will return a positive integer if version1 is older than version2
compareVersions = (version1, version2) => {
  let diff = 0;
  const splitVersion1 = version1.split('.').map(el => parseInt(el));
  const splitVersion2 = version2.split('.').map(el => parseInt(el));
  const length1 = splitVersion1.length;
  const length2 = splitVersion2.length;
  const maxLength = Math.max(length1, length2);
  for (let i = 0; i < maxLength; i++) {
    const val1 = i < length1 ? splitVersion1[i] : 0;
    const val2 = i < length2 ? splitVersion2[i] : 0;
    diff = val1 - val2;
    if (diff) {
      break;
    }
  }
  return diff;
}
